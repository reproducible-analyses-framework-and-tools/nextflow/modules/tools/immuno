#!/usr/bin/env nextflow

// Pre-processing

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

// MHC Callers

include { hlaprofiler_predict } from '../hlaprofiler/hlaprofiler.nf'

include { optitype_razers3 } from '../optitype/optitype.nf'
include { optitype_samtools_bam2fq } from '../optitype/optitype.nf'
include { optitype_sub } from '../optitype/optitype.nf'

include { arcashla_extract } from '../arcashla/arcashla.nf'
include { arcashla_genotype } from '../arcashla/arcashla.nf'

include { seq2hla } from '../seq2hla/seq2hla.nf'

// Antigen Tools

include { netmhcpan } from '../netmhcpan/netmhcpan.nf'
include { netmhcstabpan } from '../netmhcstabpan/netmhcstabpan.nf'
include { netctlpan } from '../netctlpan/netctlpan.nf'
include { antigen_garnish_make_input_file } from '../antigen.garnish/antigen.garnish.nf'
include { antigen_garnish_foreignness } from '../antigen.garnish/antigen.garnish.nf'
include { antigen_garnish_foreignness_rna } from '../antigen.garnish/antigen.garnish.nf'
include { antigen_garnish_dissimilarity } from '../antigen.garnish/antigen.garnish.nf'
include { antigen_garnish_dissimilarity_rna } from '../antigen.garnish/antigen.garnish.nf'
//include { hlathena } from '../hlathena/hlathena.nf'
include { mhcflurry_make_input_file } from '../mhcflurry/mhcflurry.nf'
include { mhcflurry_predict } from '../mhcflurry/mhcflurry.nf'
include { deephlapan_make_input_file } from '../deephlapan/deephlapan.nf'
include { deephlapan } from '../deephlapan/deephlapan.nf'

// TCR Tools

include { mixcr_align } from '../mixcr/mixcr.nf'
include { mixcr_assemble } from '../mixcr/mixcr.nf'
include { mixcr_export } from '../mixcr/mixcr.nf'
include { mixcr_shotgun } from '../mixcr/mixcr.nf'

//BCR Tools

include { vdjer } from '../vdjer/vdjer.nf'
include { vdjer_all } from '../vdjer/vdjer.nf'

// Misc.

include { samtools_index } from '../samtools/samtools.nf'

include { seqtk_sample } from '../seqtk/seqtk.nf'
include { trim_galore_hlap } from '../trim_galore/trim_galore.nf'

include { trim_galore } from '../trim_galore/trim_galore.nf'
include { trimmomatic } from '../trimmomatic/trimmomatic.nf'

include { star_index } from '../star/star.nf'
include { star_map } from '../star/star.nf'

include { symlink_fastqs } from '../utilities/utilities.nf'
include { wait_signal_1 } from '../utilities/utilities.nf'

// Agretopicity
include { make_blastp_inp_file } from '../lenstools/lenstools.nf'
include { blastp } from '../blast/blast.nf'


workflow manifest_to_mhc_alleles {
// require:
//   MANIFEST
//   params.immuno$manifest_to_mhc_alleles$fq_trim_tool
//   params.immuno$manifest_to_mhc_alleles$fq_trim_tool_parameters
//   params.immuno$manifest_to_mhc_alleles$aln_tool
//   params.immuno$manifest_to_mhc_alleles$aln_tool_parameters
//   params.immuno$manifest_to_mhc_alleles$aln_ref
//   params.immuno$manifest_to_mhc_alleles$mhc_caller_tool
//   params.immuno$manifest_to_mhc_alleles$mhc_caller_tool_parameters
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    aln_ref
    mhc_caller_tool
    mhc_caller_tool_parameters
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_mhc_alleles(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      mhc_caller_tool,
      mhc_caller_tool_parameters)
  emit:
    alleles = raw_fqs_to_mhc_alleles.out.alleles
    hlaprofiler_alleles = raw_fqs_to_mhc_alleles.out.hlaprofiler_alleles
    optitype_alleles = raw_fqs_to_mhc_alleles.out.optitype_alleles
    arcashla_alleles = raw_fqs_to_mhc_alleles.out.arcashla_alleles
    seq2hla_alleles = raw_fqs_to_mhc_alleles.out.seq2hla_alleles
}


workflow raw_fqs_to_mhc_alleles {
// require:
//   FQS
//   params.immuno$raw_fqs_to_mhc_alleles$fq_trim_tool
//   params.immuno$raw_fqs_to_mhc_alleles$fq_trim_tooltool_parameters
//   params.immuno$raw_fqs_to_mhc_alleles$aln_tool
//   params.immuno$raw_fqs_to_mhc_alleles$aln_tool_parameters
//   params.immuno$raw_fqs_to_mhc_alleles$aln_ref
//   params.immuno$raw_fqs_to_mhc_alleles$mhc_caller_tool
//   params.immuno$raw_fqs_to_mhc_alleles$mhc_caller_tool_parameters
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    aln_ref
    mhc_caller_tool
    mhc_caller_tool_parameters
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_mhc_alleles(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      mhc_caller_tool,
      mhc_caller_tool_parameters)
  emit:
    alleles = procd_fqs_to_mhc_alleles.out.alleles
    hlaprofiler_alleles = procd_fqs_to_mhc_alleles.out.hlaprofiler_alleles
    optitype_alleles = procd_fqs_to_mhc_alleles.out.optitype_alleles
    arcashla_alleles = procd_fqs_to_mhc_alleles.out.arcashla_alleles
    seq2hla_alleles = procd_fqs_to_mhc_alleles.out.seq2hla_alleles
}


workflow procd_fqs_to_mhc_alleles {
// require:
//   PROCD_FQS
//   params.immuno$procd_fqs_to_mhc_alleles$aln_tool
//   params.immuno$procd_fqs_to_mhc_alleles$aln_tool_parameters
//   params.immuno$procd_fqs_to_mhc_alleles$aln_ref
//   params.immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool
//   params.immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool_parameters
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    aln_ref
    mhc_caller_tool
    mhc_caller_tool_parameters
  main:
    multitools = ''
    alleles = Channel.empty()
    hlaprofiler_alleles = Channel.empty()
    optitype_alleles = Channel.empty()
    arcashla_alleles = Channel.empty()
    seq2hla_alleles = Channel.empty()

    if( mhc_caller_tool =~ /,/ ) {
      println "Running multiple MHC callers - use tool-specific output channels."
      multitools = 'True'
    }
    if( mhc_caller_tool =~ /hlaprofiler/ ) {
        seqtk_sample(
          procd_fqs,
          '10000000',
          '1234',
          '.hlap',
          '')
        trim_galore_hlap(
          seqtk_sample.out.subd_fqs,
          '--hardtrim5 50 --paired')
        hlaprofiler_predict(
          trim_galore_hlap.out.procd_fqs,
          mhc_caller_tool_parameters)
        hlaprofiler_alleles_to_netmhcpan_alleles(
          hlaprofiler_predict.out.calls)
        hlaprofiler_alleles_to_netmhcpan_alleles.out.alleles
          .set{ hlaprofiler_alleles }
        if( multitools != 'True' ) {
          hlaprofiler_alleles_to_netmhcpan_alleles.out.alleles
            .set{ alleles }
        }
    }
    if ( mhc_caller_tool =~ /optitype/ ) {
      if( aln_tool == '' ) {
        optitype_razers3(
          procd_fqs,
          '')
        alns_to_mhc_alleles(
          optitype_razers3.out.filtd_bams,
          'optitype',
          mhc_caller_tool_parameters)
        optitype_alleles_to_netmhcpan_alleles(
          alns_to_mhc_alleles.out.alleles)
        optitype_alleles_to_netmhcpan_alleles.out.alleles
          .set{ optitype_alleles }
        if( multitools != 'True' ) {
          optitype_alleles
            .set{ alleles }
        }
      }
    }
    if ( mhc_caller_tool =~ /arcashla/ ) {
      star_index(
        aln_ref,
        '')
      star_map(
        procd_fqs,
        star_index.out.idx_files,
        aln_tool_parameters,
        params.dummy_file)
      alns_to_mhc_alleles(
        star_map.out.alns,
        'arcashla',
        mhc_caller_tool_parameters)
      arcashla_alleles_to_netmhcpan_alleles(
        alns_to_mhc_alleles.out.alleles)
      arcashla_alleles_to_netmhcpan_alleles.out.alleles
        .set{ arcashla_alleles }
      if( multitools != 'True' ) {
        arcashla_alleles
          .set{ alleles }
      }
    }
    if ( mhc_caller_tool =~ /seq2hla/ ) {
      seq2hla(
        procd_fqs,
        mhc_caller_tool_parameters)
      seq2hla_alleles_to_netmhcpan_alleles(
        seq2hla.out.calls)
      seq2hla_alleles_to_netmhcpan_alleles.out.alleles
        .set{ seq2hla_alleles }
      if( multitools != 'True' ) {
        seq2hla_alleles
          .set{ alleles }
      }
    }
  emit:
      alleles
      hlaprofiler_alleles
      optitype_alleles
      arcashla_alleles
      seq2hla_alleles
}


workflow alns_to_mhc_alleles {
  take:
    alns
    mhc_caller_tool
    mhc_caller_tool_parameters
  main:
    if( mhc_caller_tool =~ /optitype/ ) {
      optitype_samtools_bam2fq(
        alns,
        '')
      optitype_sub(
        optitype_samtools_bam2fq.out.filtd_fqs,
        '')
      optitype_sub.out.calls.set{ alleles }
    } else if( mhc_caller_tool =~ /arcashla/ ) {
       arcashla_extract(
         alns,
         '')
       arcashla_genotype(
         arcashla_extract.out.extd_fqs,
         '')
       arcashla_genotype.out.alleles.set{ alleles }
     }
  emit:
    alleles
}


// Antigen Processing

workflow peps_and_alleles_to_antigen_stats {
// require:
//   PEPTIDES
//   ALLELES
//   params.immuno$peps_and_alleles_to_antigen_stats$antigen_tool
//   params.immuno$peps_and_alleles_to_antigen_stats$antigen_tool_parameters
//   params.immuno$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir
//   params.immuno$peps_and_alleles_to_antigen_stats$species
//   params.immuno$peps_and_alleles_to_antigen_stats$peptide_lengths
  take:
    peps
    alleles
    antigen_tool
    antigen_tool_parameters
    antigen_ref
    species
    peptide_lengths
  main:
    netmhcpan_outputs = Channel.empty()
    netmhcstabpan_outputs = Channel.empty()
    netctlpan_outputs = Channel.empty()
    antigen_garnish_foreignness_outputs = Channel.empty()
    antigen_garnish_dissimilarity_outputs = Channel.empty()
    mhcflurry_outputs = Channel.empty()
    deephlapan_outputs = Channel.empty()
    all_antigen_outputs = Channel.empty()
    peps
      .combine(alleles, by: [0])
      .set{ raw_peps_and_alleles }
    raw_peps_and_alleles
      .map{ [it[0], it[1], 'NA', it[2], it[3], it[6]] }
      .set{ single_samp_peps_and_alleles }
    single_samp_peps_and_alleles.set{ peps_and_alleles }

    if( antigen_tool =~ /,/ ) {
      println "Running multiple antigen tools - use tool-specific output channels."
      multitools = 'True'
    }

    antigen_tool_parameters = Eval.me(antigen_tool_parameters)
    antigen_ref = Eval.me(antigen_ref)

    if( antigen_tool =~ /netmhcpan/ ) {
      netmhcpan_tool_parameters = antigen_tool_parameters['netmhcpan'] ? antigen_tool_parameters['netmhcpan'] : ''
      netmhcpan(
        peps_and_alleles,
        netmhcpan_tool_parameters)
      netmhcpan.out.c1_antis
        .set{ netmhcpan_outputs }
    }
    if( antigen_tool =~ /netctlpan/ ) {
      netctlpan_tool_parameters = antigen_tool_parameters['netctlpan'] ? antigen_tool_parameters['netctlpan'] : ''
      netctlpan(
        peps_and_alleles,
        netctlpan_tool_parameters)
      netctlpan.out.c1_ctls
        .set{ netctlpan_outputs }
    }
    if( antigen_tool =~ /netmhcstabpan/ ) {
      netmhcstabpan_tool_parameters = antigen_tool_parameters['netmhcstabpan'] ? antigen_tool_parameters['netmhcstabpan'] : ''
      netmhcstabpan(
        peps_and_alleles,
        netmhcstabpan_tool_parameters)
      netmhcstabpan.out.c1_stabs
        .set{ netmhcstabpan_outputs }
    }
    if( antigen_tool =~ /antigen_garnish/ ) {
      pep_lens = antigen_tool_parameters['peptide_lengths'] ? antigen_tool_parameters['peptide_lengths'] : '8,9,10,11'
      antigen_garnish_make_input_file(
        peps_and_alleles.map{ [it[0], it[1], it[2], it[3], it[4]] },
        pep_lens)
    }
    if( antigen_tool =~ /antigen_garnish_foreignness/ ) {
      antigen_garnish_foreignness_tool_parameters = antigen_tool_parameters['antigen_garnish_foreignness'] ? antigen_tool_parameters['antigen_garnish_foreignness'] : ''
      antigen_garnish_ref = antigen_ref['antigen_garnish'] ? antigen_ref['antigen_garnish'] : ''
      antigen_garnish_foreignness(
        antigen_garnish_make_input_file.out.ag_inps,
        antigen_garnish_ref,
        species)
      antigen_garnish_foreignness.out.foreignness_files
        .set{ antigen_garnish_foreignness_outputs }
    }
    if( antigen_tool =~ /antigen_garnish_dissimilarity/ ) {
      antigen_garnish_dissimilarity_tool_parameters = antigen_tool_parameters['antigen_garnish_dissimilarity'] ? antigen_tool_parameters['antigen_garnish_dissimilarity'] : ''
      antigen_garnish_ref = antigen_ref['antigen_garnish'] ? antigen_ref['antigen_garnish'] : ''
      antigen_garnish_dissimilarity(
        antigen_garnish_make_input_file.out.ag_inps,
        antigen_garnish_ref,
        species)
      antigen_garnish_dissimilarity.out.dissimilarity_files
        .set{ antigen_garnish_dissimilarity_outputs }
    }
    if( antigen_tool =~ /mhcflurry/ ) {
      mhcflurry_tool_parameters = antigen_tool_parameters['mhcflurry'] ? antigen_tool_parameters['mhcflurry'] : ''
      mhcflurry_ref = antigen_ref['mhcflurry'] ? antigen_ref['mhcflurry'] : ''
      mhcflurry_make_input_file(
        peps_and_alleles,
        peptide_lengths)
      mhcflurry_predict(
        mhcflurry_make_input_file.out.mhcflurry_inpf,
        mhcflurry_ref,
        mhcflurry_tool_parameters)
      mhcflurry_predict.out.mhcflurry_scores
        .set{ mhcflurry_outputs }
    }
    if( antigen_tool =~ /deephlapan/ ) {
      deephlapan_tool_parameters = antigen_tool_parameters['deephlapan'] ? antigen_tool_parameters['deephlapan'] : ''
      deephlapan_make_input_file(
        peps_and_alleles,
        peptide_lengths)
      deephlapan(
        deephlapan_make_input_file.out.deephlapan_inpf,
        deephlapan_tool_parameters)
     deephlapan.out.deephlapan_scores
       .set{ deephlapan_outputs }
    }

    netmhcpan_outputs.map{ [it[0], it[3], it[4]] }
      .join(netmhcstabpan_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .join(netctlpan_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .join(antigen_garnish_foreignness_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .join(antigen_garnish_dissimilarity_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .join(mhcflurry_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .join(deephlapan_outputs.map{ [it[0], it[3], it[4]] }, by: [0, 1], remainder: true)
      .map{ [*it.asList().minus(null)] }
      .map{ [it[0], it[1], it[2..-1]] }
      .set{ all_antigen_outputs }

  emit:
    netmhcpan_outputs
    netmhcstabpan_outputs
    antigen_garnish_foreignness_outputs
    antigen_garnish_dissimilarity_outputs
    mhcflurry_outputs
    deephlapan_outputs
    all_antigen_outputs
}


process aggregate_pmhc_summaries {

  label "immuno_lenstools_container"
  tag "${dataset}/${pat_name}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), path(pmhc_tool_outputs)

  output:
  tuple val(pat_name), val(dataset), path("*pmhc_aggs.tsv"), emit: pmhc_aggs_tsv

  script:
  """
  NETMHCPAN_ARG=""
  NETCTLPAN_ARG=""
  NETMHCSTABPAN_ARG=""
  MHCFLURRY_ARG=""
  AG_DISSIM_ARG=""
  AG_FOREIGN_ARG=""
  DEEPHLAPAN_ARG=""

  if [ -f *netmhcpan.txt ]; then
    NETMHCPAN_ARG="--netmhcpan formatted.netmhcpan.txt"
    NETMHCPAN_VERSION=`grep version *netmhcpan.txt | rev | cut -f 1 -d ' ' | rev | head -1`
    echo "pos	allele	peptide	identity	netmhcpan_\${NETMHCPAN_VERSION}-score_el	netmhcpan_\${NETMHCPAN_VERSION}-perc_rank_el	netmhcpan_\${NETMHCPAN_VERSION}-score_ba	netmhcpan_\${NETMHCPAN_VERSION}-perc_rank_ba	netmhcpan_\${NETMHCPAN_VERSION}-aff_nm" > formatted.netmhcpan.txt
    grep -E "(H-2-|HLA-)" ${dataset}-${pat_name}*netmhcpan.txt | grep -v Protein | sed 's/ \\+/\\t/'g | sed 's/^\\t//g' | cut -f 1,2,3,11-17 | sed 's/\\*//g' | grep "^[0-9]"  >> formatted.netmhcpan.txt
  fi

  if [ -f *netctlpan.txt ]; then
    NETCTLPAN_ARG="--netctlpan formatted.netctlpan.txt"
    NETCTLPAN_VERSION=`grep version *netctlpan.txt | rev | cut -f 1 -d ' ' | rev | tail -n 1`
    echo "pos	identity	allele	peptide	netctlpan_\${NETCTLPAN_VERSION}-MHC	netctlpan_\${NETCTLPAN_VERSION}-TAP	netctlpan_\${NETCTLPAN_VERSION}-Cle	netctlpan_\${NETCTLPAN_VERSION}-Comb	netctlpan_\${NETCTLPAN_VERSION}-perc_rank" > formatted.netctlpan.txt
    grep -E "(H-2-|HLA-)" ${dataset}-${pat_name}*netctlpan.txt | grep -v Protein | grep -v prediction | sed 's/ \\+/\\t/'g | sed 's/^\\t//g' | cut -f 1-10 | sed 's/\\*//g' | grep "^[0-9]" | sed 's/^ \\+//g' >> formatted.netctlpan.txt
  fi

  if [ -f *netmhcstabpan.txt ]; then
    NETMHCSTABPAN_ARG="--netmhcstabpan formatted.netmhcstabpan.txt"
    NETMHCSTABPAN_VERSION=`grep "NetMHCstabpan version" *netmhcstabpan.txt | rev | cut -f 1 -d ' ' | rev | head -1`
    echo "pos	allele	peptide	identity	netmhcstabpan_\${NETMHCSTABPAN_VERSION}-stab_pred_score	netmhcstabpan_\${NETMHCSTABPAN_VERSION}-halflife_hours	netmhcstabpan_\${NETMHCSTABPAN_VERSION}-perc_rank_stab" > formatted.netmhcstabpan.txt
    grep -E "(H-2-|HLA-)" ${dataset}-${pat_name}*netmhcstabpan.txt | grep -v Protein | sed 's/ \\+/\\t/'g | sed 's/^\\t//g' | cut -f 1-8 | sed 's/\\*//g' | grep "^[0-9]"  >> formatted.netmhcstabpan.txt
  fi

  if [ -f *mhcflurry* ]; then
    MHCFLURRY_ARG="--mhcflurry formatted.mhcflurry.txt"
    MHCFLURRY_VERSION="NA"
    echo "identity	peptide	allele	mhcflurry_\${MHCFLURRY_VERSION}-aff	mhcflurry_\${MHCFLURRY_VERSION}-aff_perc	mhcflurry_\${MHCFLURRY_VERSION}-proc_score	mhcflurry_\${MHCFLURRY_VERSION}-pres_score	mhcflurry_\${MHCFLURRY_VERSION}-pres_perc" > formatted.mhcflurry.txt
    tail -n +2 ${dataset}-${pat_name}*mhcflurry* | sed 's/,/\t/g' >> formatted.mhcflurry.txt
  fi

  if [ -f *predicted* ]; then
    DEEPHLAPAN_ARG="--deephlapan formatted.deephlapan.txt"
    DEEPHLAPAN_VERSION="NA"
    echo "identity	allele	peptide	deephlapan_\${DEEPHLAPAN_VERSION}-binding_score	deephlapan_\${DEEPHLAPAN_VERSION}-immunogenic_score" > formatted.deephlapan.txt
    tail -n +2 ${dataset}-${pat_name}*predicted* | sed 's/,/	/g' >> formatted.deephlapan.txt
  fi

  if [ -f *ag_dissim.tsv ]; then
    AG_DISSIM_ARG="--ag-dissim formatted.ag_dissim.tsv"
    AG_DISSIM_VERSION="NA"
    echo "peptide	antigen.garnish_\$AG_DISSIM_VERSION-dissimilarity" > formatted.ag_dissim.tsv
    tail -n +2 ${dataset}-${pat_name}*ag_dissim.tsv | sed 's/,/\t/g' >> formatted.ag_dissim.tsv
  fi

  if [ -f *ag_foreign.tsv ]; then
    AG_FOREIGN_ARG="--ag-foreign formatted.ag_foreign.tsv"
    AG_FOREIGN_VERSION="NA"
    echo "peptide	antigen.garnish_\$AG_FOREIGN_VERSION-foreignness	antigen.garnish_\$AG_FOREIGN_VERSION-iedb_annotation" > formatted.ag_foreign.tsv
    if grep -q iedb_annotation ${dataset}-${pat_name}**ag_foreign.tsv; then
      tail -n +2 ${dataset}-${pat_name}**ag_foreign.tsv | sed 's/,/	/g' >> formatted.ag_foreign.tsv
    else
      tail -n +2 ${dataset}-${pat_name}**ag_foreign.tsv | sed 's/,/	/g'  | sed 's/\$/	NA/g' >> formatted.ag_foreign.tsv
    fi
  fi

  EXEC_PARAMS=`echo "\${NETMHCPAN_ARG} \${NETCTLPAN_ARG} \${NETMHCSTABPAN_ARG} \${MHCFLURRY_ARG} \${AG_DISSIM_ARG} \${AG_FOREIGN_ARG} \${DEEPHLAPAN_ARG}"`

  python /opt/lenstools/lenstools.py aggregate-pmhc-outputs  \${EXEC_PARAMS} -o ${dataset}-${pat_name}.pmhc_aggs.tsv
  """
}


workflow manifest_to_clonotypes {
// require:
//   MANIFEST
  take:
    manifest
  main:
    get_fastqs(
      manifest,
      params.immuno$fq_dir)
    raw_fqs_to_mixcr_clonotypes(
      get_fastqs.out.fastqs)
  emit:
    clonotypes = raw_fqs_to_mixcr_clonotypes.out.clonotypes
}


workflow raw_fqs_to_mixcr_clonotypes {
// require:
//   FQS
  take:
    fqs
  main:
    trim_galore(
      fqs,
      params.immuno$raw_fqs_to_mixcr_clonotypes$trim_galore_parameters)
    procd_fqs_to_mixcr_clonotypes(
      trim_galore.out.procd_fqs)
  emit:
    clonotypes = procd_fqs_to_mixcr_clonotypes.out.clonotypes
}


workflow procd_fqs_to_tcr_repertoire {
// require:
//   FQS
  take:
    fqs
    tcr_rep_tool
    tcr_rep_tool_parameters
  main:
    clonotypes = Channel.empty()
    tcr_rep_tool_parameters = Eval.me(tcr_rep_tool_parameters)
    if( tcr_rep_tool =~ /mixcr/ ) {
      mixcr_align_parameters = tcr_rep_tool_parameters['mixcr_align'] ? tcr_rep_tool_parameters['mixcr_align'] : ''
      mixcr_align(
        fqs,
        mixcr_align_parameters)
      vdjcas_to_mixcr_clonotypes(
        mixcr_align.out.vdjca_files,
        tcr_rep_tool_parameters)
      vdjcas_to_mixcr_clonotypes.out.clonotypes
        .set{ clonotypes }
    }
  emit:
    clonotypes
}


workflow vdjcas_to_mixcr_clonotypes {
// require:
//   VDJCAS
  take:
    vdjcas
    tcr_rep_tool_parameters
  main:
    mixcr_assemble_parameters = tcr_rep_tool_parameters['mixcr_assemble'] ? tcr_rep_tool_parameters['mixcr_assemble'] : ''
    mixcr_assemble_if_config = tcr_rep_tool_parameters['mixcr_if_config'] ? tcr_rep_tool_parameters['mixcr_if_config'] : ''
    mixcr_assemble(
      vdjcas,
      mixcr_assemble_if_config,
      mixcr_assemble_parameters)
    clns_to_mixcr_clonotypes(
      mixcr_assemble.out.cln_files,
      tcr_rep_tool_parameters)
  emit:
    clonotypes = clns_to_mixcr_clonotypes.out.clonotypes
}


workflow clns_to_mixcr_clonotypes {
// require:
//   params.immuno$clns_to_mixcr_clonotypes$clns
  take:
    cln_files
    tcr_rep_tool_parameters
  main:
    mixcr_export_parameters = tcr_rep_tool_parameters['mixcr_export'] ? tcr_rep_tool_parameters['mixcr_export'] : ''
    mixcr_export_exports = tcr_rep_tool_parameters['mixcr_exports'] ? tcr_rep_tool_parameters['mixcr_exports'] : ''
    mixcr_export(
      cln_files,
      mixcr_export_exports,
      mixcr_export_parameters)
  emit:
    clonotypes = mixcr_export.out.exported_files
}


workflow manifest_to_vdjer_calls {
// require:
//   MANIFEST
//   params.immuno$manifest_to_vdjer_calls$genome_ref
//   params.immuno$manifest_to_vdjer_calls$vdj_ref
//   params.immuno$manifest_to_vdjer_calls$chain
  take:
    manifest
    g_ref
    vdj_ref
    chain
  main:
    get_fastqs(
      manifest,
      params.immuno$fq_dir)
    raw_fqs_to_vdjer_calls(
      get_fastqs.out.fastqs,
      g_ref,
      vdj_ref,
      chain)
  emit:
    vdj_calls = raw_fqs_to_vdjer_calls.out.vdj_calls
}


workflow raw_fqs_to_vdjer_calls {
// require:
//   FQS
//   params.immuno$raw_fqs_to_vdjer_calls$genome_ref
//   params.immuno$raw_fqs_to_vdjer_calls$vdj_ref
//   params.immuno$raw_fqs_to_vdjer_calls$chain
  take:
    fqs
    g_ref
    vdj_ref
    chain
  main:
    trim_galore(
      fqs,
      params.immuno$raw_fqs_to_vdjer_calls$trim_galore_parameters)
    procd_fqs_to_vdjer_calls(
      trim_galore.out.procd_fqs,
      g_ref,
      vdj_ref,
      chain)
  emit:
    vdj_calls = procd_fqs_to_vdjer_calls.out.vdj_calls
}


workflow procd_fqs_to_vdjer_calls {
// require:
//   FQS
//   params.immuno$procd_fqs_to_vdjer_calls$genome_ref
//   params.immuno$procd_fqs_to_vdjer_calls$vdj_ref
//   params.immuno$procd_fqs_to_vdjer_calls$chain
  take:
    fqs
    g_ref
    vdj_ref
    chain
  main:
    procd_fqs_to_star_alns(
      g_ref,
      fqs)
    alns_to_vdjer_calls(
      procd_fqs_to_star_alns.out.alns,
      vdj_ref,
      chain)
  emit:
    vdj_calls = alns_to_vdjer_calls.out.vdj_calls
}


workflow alns_to_vdjer_calls {
// require:
//   ALNS
//   params.immuno$alns_to_vdjer_calls$vdj_ref
//   params.immuno$alns_to_vdjer_calls$chain
  take:
    alns
    vdj_ref
    chain
  main:
    samtools_index(alns)
    alns.join(samtools_index.out.bais, by: [0, 1, 2]).set{ alns_w_idxs }
    Channel.empty().set{ calls }
    if(chain == 'all') {
      vdjer_all(
        alns_w_idxs,
        vdj_ref,
        params.immuno$alns_to_vdjer_calls$vdjer_insert_size)
      vdjer_all.out.vdj_calls
    } else {
      vdjer(
        alns_w_idxs,
        vdj_ref,
        params.immuno$alns_to_vdjer_calls$vdjer_insert_size,
        chain)
      vdjer.out.vdj_calls.set{ calls }
    }
  emit:
    vdj_calls = calls
}


workflow manifest_to_mixcr_shotgun_clonotypes {
// require:
//   MANIFEST
  take:
    manifest
  main:
    get_fastqs(
      manifest,
      params.immuno$fq_dir)
    raw_fqs_to_mixcr_shotgun_clonotypes(
      get_fastqs.out.fastqs)
  emit:
    clonotypes = raw_fqs_to_mixcr_shotgun_clonotypes.out.clonotypes
    report = raw_fqs_to_mixcr_shotgun_clonotypes.out.report
    aligned_r1 = raw_fqs_to_mixcr_shotgun_clonotypes.out.aligned_r1
    aligned_r2 = raw_fqs_to_mixcr_shotgun_clonotypes.out.aligned_r2
}


workflow raw_fqs_to_mixcr_shotgun_clonotypes {
// require:
//   FQS
  take:
    fqs
  main:
    trim_galore(
      fqs,
      params.immuno$raw_fqs_to_mixcr_shotgun_clonotypes$trim_galore_parameters)

    procd_fqs_to_mixcr_shotgun_clonotypes(
      trim_galore.out.procd_fqs)
  emit:
    clonotypes = procd_fqs_to_mixcr_shotgun_clonotypes.out.clonotypes
    report = procd_fqs_to_mixcr_shotgun_clonotypes.out.report
    aligned_r1 = procd_fqs_to_mixcr_shotgun_clonotypes.out.aligned_r1
    aligned_r2 = procd_fqs_to_mixcr_shotgun_clonotypes.out.aligned_r2
}


workflow procd_fqs_to_mixcr_shotgun_clonotypes {
// require:
//   FQS
  take:
    fqs
  main:
    mixcr_shotgun(
      fqs,
      params.immuno$procd_fqs_to_mixcr_shotgun_clonotypes$mixcr_shotgun_parameters,
      params.immuno$procd_fqs_to_mixcr_shotgun_clonotypes$regex)
  emit:
    clonotypes = mixcr_shotgun.out.clonotypes
    report = mixcr_shotgun.out.report
    aligned_r1 = mixcr_shotgun.out.aligned_r1
    aligned_r2 = mixcr_shotgun.out.aligned_r2
}




workflow extract_alleles_from_manifest {
// Extract alleles from from a manifest channel. This is useful when MHC
// alleles are known or when working with non-human species.
//
// require:
//   ALLELES
  take:
    alleles
  main:
    Channel.fromPath(alleles).splitCsv(header: true, sep: '\t')
    .map{ row -> tuple("${row.Patient_Name}", "${row.File_Prefix}", "${row.Dataset}", "${row.Alleles}") }
    .set{ patient_alleles }
  emit:
    patient_alleles
}


process user_provided_alleles_to_netmhcpan_alleles {

  tag "${dataset}/${pat_name}/${run}"

  input:
  tuple val(pat_name), val(run), val(dataset), val(alleles)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.netmhcpan.alleles"), emit: netmhcpan_alleles

  script:
  """
  echo ${alleles} > ${dataset}-${pat_name}-${run}.netmhcpan.alleles
  """
}


process combine_peptide_fastas {

  tag "${dataset}/${pat_name}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), path(peptide_fastas)

  output:
  tuple val(pat_name), val(dataset), path("*all_peps.fa"), emit: peptide_fastas

  script:
  """
  rm *all_peps.fa || true # Remove any *all_peps.fa from previous runs.
  cat *fa > ${dataset}-${pat_name}.all_peps.fa
  """
}

process combine_nt_fastas {

  tag "${dataset}/${pat_name}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), path(nt_fastas)

  output:
  tuple val(pat_name), val(dataset), path("*all_nts.fa"), emit: nt_fastas

  script:
  """
  cat *fa > ${dataset}-${pat_name}.all_nts.fa
  """
}

process hlaprofiler_alleles_to_netmhcpan_alleles {
// require:
//   immuno$hlaprofiler_alleles_to_netmhcpan_alleles$hlaprofiler_calls

  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(hlaprofiler_calls)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.netmhcpan.alleles"), emit: alleles

  script:
  """
  grep '\\s[ABC]\\*' ${hlaprofiler_calls} |\
  cut -f 3,4 | sed 's/\\t/\\n/g' |\
  cut -f 1,2 -d ':' | sed 's/^/HLA-/' |\
  tr -d '\\*' | grep -v 'N' | sort | uniq | \
  sed -z 's/\\n/,/g' | sed 's/,\$//' |\
  sed 's/_updated//g' | sed 's/_novel//g'\
  > ${dataset}-${pat_name}-${run}.netmhcpan.alleles
  """
}

process optitype_alleles_to_netmhcpan_alleles {
// require:
//   immuno$optitype_alleles_to_netmhcpan_alleles$optitype_calls

  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(optitype_calls)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.netmhcpan.alleles"), emit: alleles

  script:
  """
  grep ^0 ${optitype_calls} |\
  grep ^0 |\
  cut -f 2,3,4,5,6,7 |\
  sed 's/^/HLA-/g' |\
  sed 's/\t/,HLA-/g' |\
  tr -d '\\*'  > ${dataset}-${pat_name}-${run}.netmhcpan.alleles
  """
}

process arcashla_alleles_to_netmhcpan_alleles {
// require:
//   immuno$optitype_alleles_to_netmhcpan_alleles$optitype_calls

  tag "${dataset}/${pat_name}/${run}"
  label "jq_container"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(arcashla_calls)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.netmhcpan.alleles"), emit: alleles

  script:
  """
  for locus in A B C; do
    jq ".\${locus}" *genotype.json |\
    grep \${locus} | \
    cut -f 1,2 -d ':' |\
    sed 's/^/HLA-/g' |\
    sed 's/[A-Z]"/"/g' |\
    sed 's/"//g' |\
    sed -z 's/\\n/,/g' |\
    sed 's/\s*//g' |\
    sed 's/\\*//g' >> ${dataset}-${pat_name}-${run}.netmhcpan.alleles.tmp
  done
  sed 's/,\$//g' ${dataset}-${pat_name}-${run}.netmhcpan.alleles.tmp > ${dataset}-${pat_name}-${run}.netmhcpan.alleles
  """
}

process seq2hla_alleles_to_netmhcpan_alleles {
// require:
//   immuno$optitype_alleles_to_netmhcpan_alleles$optitype_calls

  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(seq2hla_calls)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.netmhcpan.alleles"), emit: alleles

  script:
  """
  tail -n +2 ${seq2hla_calls} |\
  cut -f 2,4 |\
  sed 's/	/ /g' |\
  sed -z 's/\\n/ /g' |\
  sed 's/ /,/g' |\
  sed 's/,\$//g' |\
  sed 's/,/,HLA-/g' |\
  sed 's/^/HLA-/g' |\
  sed "s/'//g" |\
  sed 's/\\*//g' > ${dataset}-${pat_name}-${run}.netmhcpan.alleles
  """
}

process extract_matched_wt_peps {
  // The sequence identifier is the mutant peptide to be matched to the
  // standard LENS pMHC outputs.
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), path(blastp_hits)

  output:
  tuple val(pat_name), val("NA"), val(dataset), path("*.mut_wt_peps.fa"), emit: mut_wt_peps

  script:
  """
  cut -f 1,13 ${blastp_hits} | sed 's/^/>/g' | sed -z 's/\\t/\\n;\\n/g' > ${dataset}-${pat_name}.mut_wt_peps.fa
  """
}

workflow calculate_agretopicity {

  take:
    pmhcs
    blastp_data_dir
    species
    alleles
    antigen_tool
    antigen_tool_parameters
    antigen_ref
    peptide_lengths
  main:
    make_blastp_inp_file(
      pmhcs)
    blastp(
      make_blastp_inp_file.out.blastp_inp_files,
      blastp_data_dir,
      '-max_target_seqs 1 -outfmt "6 std sseq"',
      species)
    extract_matched_wt_peps(
      blastp.out.blastp_hits)
    peps_and_alleles_to_antigen_stats(
      extract_matched_wt_peps.out.mut_wt_peps,
      alleles,
      antigen_tool,
      antigen_tool_parameters,
      antigen_ref,
      species,
      peptide_lengths)
    aggregate_pmhc_summaries(
      peps_and_alleles_to_antigen_stats.out.all_antigen_outputs)
    pmhcs
      .join(aggregate_pmhc_summaries.out.pmhc_aggs_tsv)
      .join(extract_matched_wt_peps.out.mut_wt_peps.map{ [it[0], it[2], it[3]] })
      .map{ [it[0], it[1], it[2], it[4], it[6]] }
      .set{ pmhcs_with_wt_bas }
    add_agreto_metadata(
      pmhcs_with_wt_bas)
  emit:
    pmhcs_with_agretos = add_agreto_metadata.out.pmhcs_with_agretos
}


process add_agreto_metadata {

  label "immuno_lenstools_container"
  tag "${dataset}/${pat_name}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), path(pmhcs), path(wt_bas), path(mt_wt_peps)

  output:
  tuple val(pat_name), val(dataset), path("*agreto.tsv"), emit: pmhcs_with_agretos

  script:
  """
  grep -v ';' $mt_wt_peps >  mt_wt_peps.no_comms.fa
  APMHCS=`echo ${pmhcs}`
  python /opt/lenstools/lenstools.py add-agreto-metadata -p ${pmhcs} -a ${wt_bas} -m mt_wt_peps.no_comms.fa -o \${APMHCS%.tsv}.agreto.tsv
  """
}

workflow parse_immuno_manifest {
// This is a modified version of parse_manifest from the
// utilities module. It is modified to also extract a comma-separated list of
// MHC alles for procesing through specific workflows.
//
// Consume a manifest emitted by a channel and convert it to a format suitable
// for processing. This typically involves selecting only a handful of required
// columns and filtering based on Sequencing_Method.
//
// take:
//   raw_manifest -  Raw manifest emitted by a dataset preparation step.
//   molecule_filter - A regular expression used for filtering samples (e.g. '^RNA')
//   separator - Delimiter used to parse rows of raw_manifest
//
// emit:
//   manifest - Formatted manifest ready for processing (by manifest_to_* steps)

// require:
//   MANIFEST
//   params.immuno$parse_immuno_manifest$molecule_filter
//   params.immuno$parse_immuno_manifest$separator
  take:
    raw_manifest
    molecule_filter
    separator
  main:
    symlink_fastqs(raw_manifest, params.global_fq_dir, params.fq_dir)

    raw_manifest.splitCsv(header: true, sep: separator)
    .map{ row -> tuple("${row.Patient_Name}", "${row.Run_Name}", "${row.Dataset}", "${row.File_Prefix}", "${row.Sequencing_Method}", "${row.Normal}", "${row.Alleles}") }
    .filter{ it[4] =~ /${molecule_filter}/ }
    .set{ manifest }

    wait_signal_1(manifest.toList(), symlink_fastqs.out.unlock)
  emit:
    manifest = wait_signal_1.out.e_output.flatMap{ it -> it }
}

workflow parse_generic_immuno_manifest {
// Consume a manifest emitted by a channel and convert it to a format suitable
// for processing. This typically involves selecting only a handful of required
// columns and filtering based on Sequencing_Method.
//
// take:
//   raw_manifest -  Raw manifest emitted by a dataset preparation step.
//   molecule_filter - A regular expression used for filtering samples (e.g. '^RNA')
//   separator - Delimiter used to parse rows of raw_manifest
//
// emit:
//   manifest - Formatted manifest ready for processing (by manifest_to_* steps)

// require:
//   MANIFEST
//   params.utilities$parse_generic_manifest$separator
  take:
    raw_manifest
    separator
  main:
//    symlink_inputs(raw_manifest, params.global_inp_dir, params.inp_dir)
    symlink_inputs(raw_manifest, params.global_fq_dir, params.fq_dir)

    raw_manifest.splitCsv(header: true, sep: separator)
    .map{ row -> tuple("${row.Patient_Name}", "${row.Run_Name}", "${row.Dataset}", "${row.File_Prefix}", "${row.Sequencing_Method}", "${row.Normal}", "${row.Allele}") }
    .set{ manifest }

    wait_signal_1(manifest.toList(), symlink_inputs.out.unlock)
  emit:
    manifest = wait_signal_1.out.e_output.flatMap{ it -> it }
}
